--------------------------------------------------------------------------------
--common Global variables
turn_around_timer = 0
grab_timer = 0
count_time = 0
zero_msg = {0,0,0,0,0,0,0,0}
received_message = {}
parent = {}
root = 0
--children = {}
wait_timer = 0
local m = require "matrix"
rs_small_box = m{{1,1,0}}
rs_large_box = m{{0,1,1}}
rs_small_disc = m{{1,0,0}}
rs_large_disc = m{{1,1,1}}
req_skills = m(1,3,0)
chk_skill = m(1,3,0)
--Specific Global variables
my_skill = m{{1,1,0}}
price = 15
alpha = 0.7
beta = 0.3
--Core functions
function init()
  self_addr = addr(robot.id)
  log(robot.id," = ",id)
  state = "search"
  prev_state = "dummy"
  int_state = "listening"
  prev_int_state = "dummy"
  robot.colored_blob_omnidirectional_camera.enable()
end
--------------------------------------------------------------------------------
function step()
  if state ~= prev_state then
    log(self_addr,"=",state)
  end
  prev_state = state
  if state == "search" then
    robot.leds.set_all_colors(0,0,0)
    search()
  elseif state == "choose" then
    robot.leds.set_all_colors(0,0,255)
    choose()
  elseif state == "approach" then
    robot.leds.set_all_colors(0,0,255)
    approach()
  elseif state == "decide_req_skill" then
    robot.leds.set_all_colors(0,0,255)
    decide_req_skill()
  elseif state == "leave_task" then
    robot.leds.set_all_colors(0,0,0)
    leave_task()
  elseif state == "grab" then
    robot.leds.set_all_colors(0,0,255)
    grab()
  elseif state == "root_bcast" then
    robot.leds.set_all_colors(255,0,255)
    root_bcast()
  elseif state == "waiting" then
    robot.leds.set_all_colors(255,0,255)
    waiting()
  elseif state == "available_table" then
    robot.leds.set_all_colors(255,0,255)
    available_table()
  elseif state == "decide_team" then
    robot.leds.set_all_colors(255,0,255)
    decide_team()
  elseif state == "specific_call" then
  robot.leds.set_all_colors(255,0,255)
  specific_call()
  elseif state == "waiting_for_team" then
    robot.leds.set_all_colors(255,0,255)
    waiting_for_team()
  elseif state == "dest_orient_determine" then
    robot.leds.set_all_colors(255,0,255)
    dest_orient_determine()
  elseif state == "go_home" then
    robot.leds.set_all_colors(255,0,255)
    go_home()
  elseif state == "home" then
    robot.leds.set_all_colors(255,0,255)
    home()
  elseif state == "disband" then
    disband()
  end
end
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
---------------------------------help functions---------------------------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
function leave_check()
  leave_check_flag = 1
  if #robot.colored_blob_omnidirectional_camera == 0 and #robot.colored_blob_perspective_camera == 0 then
    return(1)
  else
    for i = 1, #robot.colored_blob_omnidirectional_camera do
      if object == "large_disc" then
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 165
        and robot.colored_blob_omnidirectional_camera[i].color.green == 42
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 42) then
          leave_check_flag = 0
        end
      elseif object == "small_disc" then
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 255
        and robot.colored_blob_omnidirectional_camera[i].color.green == 255
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then
          leave_check_flag = 0
        end
      elseif object == "small_box" then
        if(robot.colored_blob_omnidirectional_camera[i].color.red == 160
        and robot.colored_blob_omnidirectional_camera[i].color.green == 32
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then
          leave_check_flag = 0
        end
      elseif object == "large_box" then
        if(robot.colored_blob_omnidirectional_camera[i].color.red == 255
        and robot.colored_blob_omnidirectional_camera[i].color.green == 140
        and robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
          leave_check_flag = 0
        end
      end
    end
    return(leave_check_flag)
  end
end
--------------------------------------------------------------------------------
function check_skill()
  if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
          if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 42)  then
                  chk_skill = rs_large_disc
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then
                  chk_skill =  rs_small_disc
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then
                  chk_skill = rs_small_box
          elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
              robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
              robot.colored_blob_omnidirectional_camera[i].color.blue == 0) then
                  chk_skill = rs_small_box
          end
      end
  end
  chk_flag = 0
  for i = 1, 6 do
    if chk_skill[1][i] == my_skill[1][i] then
      check_flag = 1
    end
  end
  if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 0 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) or
          (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0)then
              check_flag = 0
          end
        end
      end
  return(check_flag)
end
--------------------------------------------------------------------------------
function flush(t)
  while #t ~= 0 do rawset(t, #t, nil) end
end
--------------------------------------------------------------------------------
function ReverseTable(t)
    local reversedTable = {}
    local itemCount = #t
    for k, v in ipairs(t) do
        reversedTable[itemCount + 1 - k] = v
    end
    return reversedTable
end
--------------------------------------------------------------------------------
function compose_dist(to_addr, meaning, degree, distance, skill)
  message = {}
  message[1] = self_addr
  message[2] = to_addr
  message[3] = meaning
  message[4] = degree
  message[5] = distance
  for i = 1, 3 do
    table.insert(message,skill[1][i])
  end
  return(message)
end
--------------------------------------------------------------------------------
function my_entry(d)
  self_entry = m(1,5,0)
  self_entry[1][1] = self_addr
  self_entry[1][2] = d
  self_entry[1][3] = my_skill[1][1]
  self_entry[1][4] = my_skill[1][2]
  self_entry[1][5] = my_skill[1][3]
  self_entry[1][6] = price -- basic price
  self_entry[1][7] = alpha -- remaining battery coeff
  self_entry[1][8] = beta  -- rate of discharge
  return(self_entry)
end
--------------------------------------------------------------------------------
function receive(r)
  if #received_message == 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr
        and robot.range_and_bearing[i].data[3] == r then

        table.insert(received_message, robot.range_and_bearing[i])
      end
    end
  elseif #received_message > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr
          and robot.range_and_bearing[i].data[3] == r then
        receive_flag = 0
        for j = 1, #received_message do
          if robot.range_and_bearing[i].data[1] == received_message[j].data[1] then
            receive_flag = 1
          end
        end
        if receive_flag == 0 then
          table.insert(received_message, robot.range_and_bearing[i])
        end
      end
    end
  end
end
--------------------------------------------------------------------------------
function create_matrix()
  create_matrix_flag = 0
  for i = 1, 3 do
    if req_skills[1][i] ~= 0 and req_skills[1][i] == my_skill[1][i] then
      create_matrix_flag = 1
    end
  end
  if create_matrix_flag == 1 then -- skills match with req skill add our skill to file
    p_t = table.load(parent_file)
    rows = #p_t
    collumns = #p_t[1]
    p_m = m(rows,collumns,0)
    for i = 1, rows do
      for j = 1, collumns do
        p_m[i][j] = p_t[i][j]
      end
    end
    created_matrix = m.concatv(p_m, entry)
  elseif create_matrix_flag == 0 then -- skills don't match with our skills
    p_t = table.load(parent_file)
    rows = #p_t
    collumns = #p_t[1]
    p_m = m(rows,collumns,0)
    for i = 1, rows do
      for j = 1, collumns do
        p_m[i][j] = p_t[i][j]
      end
    end
    created_matrix = p_m
  end
  --print(created_matrix)
  for i = 1, #created_matrix do
    if created_matrix[i][2] == 0 then
      root = created_matrix[i][1]
    end
  end
  return(created_matrix)
end
--------------------------------------------------------------------------------
function receive_skill()
  for i = 1, #robot.range_and_bearing do
    if (robot.range_and_bearing[i].data[2] == self_addr
    and robot.range_and_bearing[i].data[3] == 3) then
      if #received_message == 0 then
        table.insert(received_message, robot.range_and_bearing[i])
      else
        received_flag = 0
        for j = 1, #received_message do
          if robot.range_and_bearing[i].data[1] == received_message[j].data[1] then
            received_flag = 1
          end
        end
        if received_flag == 0 then
          table.insert(received_message, robot.range_and_bearing[i])
        end
      end
    end
  end
end
--------------------------------------------------------------------------------
function compose(to,m)
  message = {}
  message[1] = self_addr
  message[2] = to
  message[3] = m
  message[4] = 0
  message[5] = 0
  message[6] = 0
  message[7] = 0
  message[8] = 0
  return(message)
end
--------------------------------------------------------------------------------
--------------------------------State functions---------------------------------
--------------------------------------------------------------------------------
function search()
  if int_state ~= prev_int_state then
        log(self_addr,"=",int_state)
  end
  prev_int_state = int_state
  if int_state == "listening" then
    robot.range_and_bearing.set_data(zero_msg)
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value
    --[[if bat_cur <= (30/100) * bat_total then
        state = "to_charge"
    end]]
    if sensingLeft ~= 0 then
          robot.wheels.set_velocity(7,3)
    elseif sensingRight ~= 0 then
          robot.wheels.set_velocity(3,7)
    else
          robot.wheels.set_velocity(10,10)
    end
    if #robot.colored_blob_omnidirectional_camera ~= 0  then
      for i = 1,#robot.colored_blob_omnidirectional_camera do
        check_flag = check_skill()
        if (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 42) and check_flag == 1  then
            state = "choose"
            object = "large_disc"
            log(object)
        elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255) and check_flag == 1 then
            state = "choose"
            object = "small_disc"
            log(object)
        elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 160 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 240) and check_flag == 1 then
            state = "choose"
            object = "small_box"
            log(object)
        elseif(robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
            robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 0) and check_flag == 1 then
            state = "choose"
            object = "large_box"
            log(object)
        end
      end
    end
    calls = {}
    if #robot.range_and_bearing > 0 then
      for i = 1,#robot.range_and_bearing do
        if robot.range_and_bearing[i].data[2] == 255
        and robot.range_and_bearing[i].data[3] == 1
        and robot.range_and_bearing[i].range < 200 then
          table.insert(calls, robot.range_and_bearing[i])
        end
      end
    end
    if #calls > 0 then
      distance = 10000
      caller = 0
      for i = 1, #calls do
        if calls[i].range < distance then
          distance = calls[i].range
          caller = calls[i]
        end
      end
    end
    if #calls > 0 and caller ~= 0 then
      int_state = "responding_to_request"
    end
  elseif int_state == "responding_to_request" then
    responding_to_request()
  elseif int_state == "check_robust" then
    check_robust()
  elseif int_state == "sec_bcast" then
    sec_bcast()
  elseif int_state == "waiting_for_table" then
    waiting_for_table()
  elseif int_state == "recompose_matrix" then
    recompose_matrix()
  elseif int_state == "retrace" then
    retrace()
  elseif int_state == "waiting_for_final_call" then
  waiting_for_final_call()
elseif int_state == "approach_caller" then
  approach_caller()
elseif int_state == "grab_robot" then
  robot.leds.set_all_colors(255,0,255)
  grab_robot()
elseif int_state == "final_orientation" then
  robot.leds.set_all_colors(255,0,255)
  final_orientation()
elseif int_state == "home" then
  robot.leds.set_all_colors(255,0,255)
  home()
  end
end
--------------------------------------------------------------------------------
function choose()
  if #robot.colored_blob_omnidirectional_camera == 0 then
      state = "search"
      --log(1)
  else
      --log(2)
      robot.wheels.set_velocity(0,0)
      closest = robot.colored_blob_omnidirectional_camera[1]
      dist = robot.colored_blob_omnidirectional_camera[1].distance
      ang =  robot.colored_blob_omnidirectional_camera[1].angle
      for i = 1, #robot.colored_blob_omnidirectional_camera do

          if (robot.colored_blob_omnidirectional_camera[i].color.red == 165
          and robot.colored_blob_omnidirectional_camera[i].color.green == 42
          and robot.colored_blob_omnidirectional_camera[i].color.blue == 42)
          or(robot.colored_blob_omnidirectional_camera[i].color.red == 255
          and robot.colored_blob_omnidirectional_camera[i].color.green == 255
          and robot.colored_blob_omnidirectional_camera[i].color.blue == 255)
          or(robot.colored_blob_omnidirectional_camera[i].color.red == 160
          and robot.colored_blob_omnidirectional_camera[i].color.green == 32
          and robot.colored_blob_omnidirectional_camera[i].color.blue == 240)
          or(robot.colored_blob_omnidirectional_camera[i].color.red == 255
          and robot.colored_blob_omnidirectional_camera[i].color.green == 140
          and robot.colored_blob_omnidirectional_camera[i].color.blue == 0)then
              closest = robot.colored_blob_omnidirectional_camera[i]
              dist = closest.distance
              ang = closest.angle
          end
      end
      if ang > 0.1 then
          robot.wheels.set_velocity(-1,1)
      elseif ang < -0.1 then
          robot.wheels.set_velocity(1,-1)
      elseif ang >= -0.1 and ang <= 0.1 then
          state = "approach"
      end
  end
end
--------------------------------------------------------------------------------
function approach()
  if #robot.colored_blob_omnidirectional_camera > 0 then
    x = 0
    for i = 1, 24 do --some modification must be done here as we need not check
                         --all proximity sensors then ones located in front shall do
      if x < robot.proximity[i].value then
          x = robot.proximity[i].value
      end
    end--trying to keep the orientation while approaching the obstacle------------
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle

        for i = 1, #robot.colored_blob_omnidirectional_camera do
            if dist > robot.colored_blob_omnidirectional_camera[i].distance and
                (robot.colored_blob_omnidirectional_camera[i].color.red == 255 or
                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                ang = robot.colored_blob_omnidirectional_camera[i].angle

            end
        end
        if ang > 0 then
            robot.wheels.set_velocity(5,6)
        end
        if ang < 0 then
            robot.wheels.set_velocity(6,5)
        end
        if ang == 0 then
            robot.wheels.set_velocity(5,5)
        end--trying to slow down when reaching near the obstacle----------------
        if x >= 0.5 then
            robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
        end
        if x >= 0.9 then
            robot.wheels.set_velocity(0,0)
            state = "decide_req_skill"
        end
    else
        state = "search"
    end
end
--------------------------------------------------------------------------------
function decide_req_skill()
  if object == "small_box" then
    req_skills = rs_small_box
  elseif object == "large_box" then
    req_skills = rs_large_box
  elseif object == "small_disc" then
    req_skills = rs_small_disc
  elseif object == "large_disc" then
    req_skills = rs_large_disc
  end
  decide_req_skill_flag = 0
  for i = 1, 3 do
    if req_skills[1][i] == my_skill[1][i] and req_skills[1][i] == 1 then
      decide_req_skill_flag = 1
    end
  end
  if decide_req_skill_flag == 1 then
    state = "grab"
  elseif decide_req_skill_flag == 0 then
    robot.gripper.unlock()
    state = "leave_task"
  end
end
--------------------------------------------------------------------------------
function leave_task()
  robot.range_and_bearing.set_data(zero_msg)
  if turn_around_timer < 35 then
    robot.wheels.set_velocity(5,-5)
    turn_around_timer = turn_around_timer + 1
  else
    robot.wheels.set_velocity(10,10)
  end
  leave_task_flag = leave_check()
  if leave_task_flag == 1 then
    state = "search"
    turn_around_timer = 0
  end
end
--------------------------------------------------------------------------------
function grab()
  --log(grab_timer)
  grab_timer = grab_timer + 1
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value >= 0.92 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if grab_timer >= 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        grab_timer = 0
        state = "root_bcast"
    end
end
--------------------------------------------------------------------------------
function root_bcast()
  wait_timer = wait_timer + 1
  degree = 0
  entry = my_entry(degree)
  my_file = "R"..tostring(self_addr)..".lua"
  table.save(entry,my_file)
  root_broadcast = compose_dist(255, 1, degree, 2, req_skills)
  robot.range_and_bearing.set_data(root_broadcast)
  receive(2)
  if wait_timer > 10 and #received_message == 0 then
    wait_timer = 0
    robot.gripper.unlock()
    state = "leave_task"
  elseif wait_timer > 10 and #received_message > 0 then
    wait_timer = 0
    children = #received_message
    state = "waiting"
    flush(received_message)
  end
end
--------------------------------------------------------------------------------
function waiting()
  receive_skill()
  if #received_message == children then
    state = "available_table"
  end
end
--------------------------------------------------------------------------------
function available_table()
  to_add_flag = 0
  our_file = "R"..tostring(self_addr)..".lua"
  our_info = table.load(our_file)
  our_row = #our_info
  our_coll = #our_info[1]
  our_matrix = m(our_row, our_coll, 0)
  for i = 1, our_row do
    for j = 1, our_coll do
      our_matrix[i][j] = our_info[i][j]
    end
  end
  for i = 1, #received_message do
    to_add_file = "R"..tostring(received_message[i].data[1])..".lua"
    to_add_info = table.load(to_add_file)
    add_rows = #to_add_info
    add_coll = #to_add_info[1]
    to_add_matrix = m(add_rows, add_coll, 0)
    for j = 1, add_rows do
      for k = 1, add_coll do
        to_add_matrix[j][k] = to_add_info[j][k]
      end
    end
    for j = 1, add_rows do
      to_add_robo = to_add_matrix[j][1]
      to_add_flag = 0
      for k = 1, #our_matrix do
        if our_matrix[k][1] == to_add_robo then
          to_add_flag = 1
        end
      end
      if to_add_flag == 0 then -- should be added
        add_matrix = m(1,8,0)
        for l = 1, 8 do
          add_matrix[1][l] = to_add_matrix[j][l]
        end
        our_matrix = m.concatv(our_matrix, add_matrix)
      end
    end
  end
  flush(received_message)
  table.save(our_matrix, our_file)
  state = "decide_team"
end
--------------------------------------------------------------------------------
function decide_team()
  our_file = "R"..tostring(self_addr)..".lua"
  our_info = table.load(our_file)
  our_row = #our_info
  our_coll = #our_info[1]
  our_matrix = m(our_row, our_coll, 0)
  for i = 1, our_row do
    for j = 1, our_coll do
      our_matrix[i][j] = our_info[i][j]
    end
  end
  --adding costs
  c = m(our_row, 1, 0)
  our_matrix = m.concath(our_matrix, c)
  for i = 1, our_row do
    our_matrix[i][9] = (our_matrix[i][6]/ our_matrix[i][7]) + (our_matrix[i][2] * 20 * our_matrix[i][8])
  end
  cost_table = {}
  costs = {}
  for i = 1, our_row do
    if #costs == 0 then
      table.insert(costs, our_matrix[i][9])
    elseif #costs > 0 then
      costs_flag = 0
      for j = 1, #costs do
        if costs[j] == our_matrix[i][9] then
          costs_flag = 1
        end
      end
      if costs_flag == 0 then
        table.insert(costs,our_matrix[i][9])
      end
    end
  end
  --sorting according to costs
  table.sort(costs)
  table.insert(cost_table,our_matrix[1])
  for i = 1, #costs do
    for j = 2, #our_matrix do
      if costs[i] == our_matrix[j][9] then
        table.insert(cost_table, our_matrix[j])
      end
    end
  end
  cost_matrix = m(#cost_table, #cost_table[1], 0)
  for i = 1, #cost_table do
    for j = 1, #cost_table[1] do
      cost_matrix[i][j] = cost_table[i][j]
    end
  end
  --robustness checking
  dyn_req_skills = m(1,3,0)
  for i = 1, 3 do
    dyn_req_skills[1][i] = root_broadcast[i+5]
  end
  for i = 1, 3 do
    dyn_req_skills[1][i] = dyn_req_skills[1][i]*2
  end
  for i = 3, 5 do
    if dyn_req_skills[1][i-2] > 0  and cost_matrix[1][i] == 1 then
      dyn_req_skills[1][i-2] = dyn_req_skills[1][i-2] - 1
    end
  end
  --adding root
  team_matrix = m(1, 9, 0)
  for i = 1, 9 do
    team_matrix[1][i] = our_matrix[1][i]
  end
  for i = 2, #cost_matrix do
    add_flag = 0
    orig_dyn_req_skills = m(1,3,0)
    for i = 1, 3 do
      orig_dyn_req_skills[1][i] = dyn_req_skills[1][i]
    end
    for j = 3, 5 do
      if dyn_req_skills[1][j-2] > 0 and cost_matrix[i][j] == 1 then
        dyn_req_skills[1][j-2] = dyn_req_skills[1][j-2] - 1
      end
    end
    for j = 1, 3 do
      if dyn_req_skills[1][j] ~= orig_dyn_req_skills[1][j] then
        add_flag = 1
      end
    end
    if add_flag == 1 then
      add_matrix = m(1,9,0)
      for j = 1, 9 do
        add_matrix[1][j] = cost_matrix[i][j]
      end
      log(i)
      team_matrix = m.concatv(team_matrix, add_matrix)
    end
    if dyn_req_skills[1][1] == 0 and dyn_req_skills[1][2] == 0 and dyn_req_skills[1][3] == 0 then
      break
    end
  end
  print("req_skills")
  print(req_skills)
  ans1 = m(1,9,0)
  ans1[1][1] = "r_id"
  ans1[1][2] = "deg"
  ans1[1][3] = "S1"
  ans1[1][4] = "S2"
  ans1[1][5] = "S3"
  ans1[1][6] = "P"
  ans1[1][7] = "a"
  ans1[1][8] = "b"
  ans1[1][9] = "Cost"
  ans1 = m.concatv(ans1, cost_matrix)
  print("cost matrix")
  print(ans1)
  ans2 = m(1,9,0)
  ans2[1][1] = "r_id"
  ans2[1][2] = "deg"
  ans2[1][3] = "S1"
  ans2[1][4] = "S2"
  ans2[1][5] = "S3"
  ans2[1][6] = "P"
  ans2[1][7] = "a"
  ans2[1][8] = "b"
  ans2[1][9] = "Cost"
  ans2 = m.concatv(ans2, team_matrix)
  print("robust team")
  print(ans2)
  if dyn_req_skills[1][1] == 0 and dyn_req_skills[1][2] == 0 and dyn_req_skills[1][3] == 0 then
    state = "specific_call"
  else
    state = "disband"
  end
end
--------------------------------------------------------------------------------
function specific_call()
  log("s")
  specific_msg = {}
  specific_msg[1] = self_addr
  specific_msg[2] = 4
  log(specific_msg[2])
  for i = 2, #team_matrix do
    table.insert(specific_msg, team_matrix[i][1])
  end
  n = #specific_msg
  if n < 8 then
    for i = 1, 8 - n do
      table.insert(specific_msg, 0)
    end
  end
  robot.range_and_bearing.set_data(specific_msg)
  state = "waiting_for_team"
end
--------------------------------------------------------------------------------
function waiting_for_team()
  receive(6)
  if #received_message == children then
    state = "dest_orient_determine"
    flush(received_message)
  end
end
--------------------------------------------------------------------------------
function dest_orient_determine()
  --log(object)
  if object == "large_box" then --large_box to north
    orient_msg = compose(255,7)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "large_disc" then --go south
    orient_msg = compose(255,9)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "small_disc" then --go west
    orient_msg = compose(255,8)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  elseif object == "small_box" then --go east
    orient_msg = compose(255,10)
    robot.range_and_bearing.set_data(orient_msg)
    state = "go_home"
  end
end
--------------------------------------------------------------------------------
function go_home()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if orient_msg[3] == 7 then
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 8 then
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 9 then
        if angle > 3 and angle < 3.2 then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 10 then
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)  -- robot.wheels.set_velocity(1,-1)

        end
    end
    if #robot.range_and_bearing then
      receive(11)
      if #received_message == children then
        lets_go_msg = compose(255,12,empty_skill)
        robot.range_and_bearing.set_data(lets_go_msg)
        go_ahead = 0
        state ="home"
        flush(received_message)
      end
    end
end
--------------------------------------------------------------------------------
function disband()
  disband_msg = {}
  disband_msg[1] = self_addr
  disband_msg[2] = 5
  for i = 3, 8 do
    table.insert(disband_msg, 0)
  end
  robot.range_and_bearing.set_data(disband_msg)
  robot.gripper.unlock()
  state = "leave_task"
end
--------------------------------------------------------------------------------
--------------------Non-Initiator/ int_state functions--------------------------
--------------------------------------------------------------------------------
function responding_to_request()
  robot.wheels.set_velocity(0,0)
  if #robot.range_and_bearing > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == 255
      and robot.range_and_bearing[i].data[3] == 1
      and robot.range_and_bearing[i].range < 200 then

        degree = robot.range_and_bearing[i].data[4] + 1
        req_skills[1][1] = robot.range_and_bearing[i].data[6]
        req_skills[1][2] = robot.range_and_bearing[i].data[7]
        req_skills[1][3] = robot.range_and_bearing[i].data[8]
        entry = my_entry(degree)
        parent = robot.range_and_bearing[i]
        parent_file = "R"..tostring(parent.data[1])..".lua"
        my_file = "R"..tostring(self_addr)..".lua"
        my_matrix = create_matrix()
        table.save(my_matrix, my_file)
        ack_to_parent = compose_dist(parent.data[1], 2, degree, 2, req_skills)
        robot.range_and_bearing.set_data(ack_to_parent)
        int_state = "check_robust"
      end
    end
  end
end
--------------------------------------------------------------------------------
function check_robust()
  rows = #my_matrix
  sum = m(1,3,0)
  for j = 3, 5 do
    for i = 1, rows do
      sum[1][j-2] = sum[1][j-2] + my_matrix[i][j]
    end
  end
  check_robust_flag = 0
  for i = 1, 3 do
    if sum[1][i] < 2 and req_skills[1][i] == 1 then
      check_robust_flag = 1
    end
  end
  if check_robust_flag == 1 then --not_Robust
    int_state = "sec_bcast"
  elseif check_robust_flag == 0 then --Robust
    int_state = "retrace"
  end
end
--------------------------------------------------------------------------------
function sec_bcast()
  robot.wheels.set_velocity(0,0)
  sec_broadcast = compose_dist(255, 1, degree, 2, req_skills)
  robot.range_and_bearing.set_data(sec_broadcast)
  wait_timer = wait_timer + 1
  receive(2)
  if wait_timer > 10 and #received_message == 0 then
    wait_timer = 0
    int_state = "retrace"
  elseif wait_timer > 10 and #received_message > 0 then
    wait_timer = 0
    children = #received_message
    int_state = "waiting_for_table"
    flush(received_message)
  end
end
--------------------------------------------------------------------------------
function waiting_for_table()
  receive_skill()
  if #received_message == children then
    int_state = "recompose_matrix"
  end
end
--------------------------------------------------------------------------------
function recompose_matrix()
  to_add_flag = 0
  our_file = "R"..tostring(self_addr)..".lua"
  our_info = table.load(our_file)
  our_row = #our_info
  our_coll = #our_info[1]
  our_matrix = m(our_row, our_coll, 0)
  for i = 1, our_row do
    for j = 1, our_coll do
      our_matrix[i][j] = our_info[i][j]
    end
  end
  for i = 1, #received_message do
    to_add_file = "R"..tostring(received_message[i].data[1])..".lua"
    to_add_info = table.load(to_add_file)
    add_rows = #to_add_info
    add_coll = #to_add_info[1]
    to_add_matrix = m(add_rows, add_coll, 0)
    for j = 1, add_rows do
      for k = 1, add_coll do
        to_add_matrix[j][k] = to_add_info[j][k]
      end
    end
    for j = 1, add_rows do
      to_add_robo = to_add_matrix[j][1]
      to_add_flag = 0
      for k = 1, #our_matrix do
        if our_matrix[k][1] == to_add_robo then
          to_add_flag = 1
        end
      end
      if to_add_flag == 0 then -- should be added
        add_matrix = m(1,8,0)
        for l = 1, 8 do
          add_matrix[1][l] = to_add_matrix[j][l]
        end
        our_matrix = m.concatv(our_matrix, add_matrix)
      end
    end
  end
  flush(received_message)
  table.save(our_matrix, our_file)
  int_state = "retrace"
end
--------------------------------------------------------------------------------
function retrace()
  retrace_to_parent = compose_dist(parent.data[1], 3, degree, 2, req_skills)
  robot.range_and_bearing.set_data(retrace_to_parent)
  int_state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
function waiting_for_final_call()
  count_time = count_time + 1
  if #robot.range_and_bearing > 0 then
    called_flag = 0
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[1] == root
      and robot.range_and_bearing[i].data[2] == 4 then
        for j = 3, 8 do
          if robot.range_and_bearing[i].data[j] == self_addr then
            called_flag = 1
            int_state = "approach_caller"
          end
        end
        if called_flag == 0 then
          int_state = "listening"
          state = "search"
        end
      elseif robot.range_and_bearing[i].data[1] == root
      and robot.range_and_bearing[i].data[2] == 5 then
        int_state = "listening"
        state = "search"
      end
    end
  end
  if count_time > 700 then
    int_state = "listening"
    state = "search"
    count_time = 0
  end
end
--------------------------------------------------------------------------------
function approach_caller()
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value

    if #robot.range_and_bearing then

        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == 4 and
               robot.range_and_bearing[i].data[1] == root then

                if robot.range_and_bearing[i].range > 75 then

                    if sensingLeft ~= 0 then
                        robot.wheels.set_velocity(7,3)
                    elseif sensingRight ~= 0 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing > 0 and
                            robot.range_and_bearing[i].horizontal_bearing <= 0.5 then
                        robot.wheels.set_velocity(9,10)
                    elseif robot.range_and_bearing[i].horizontal_bearing >0.5 then
                        robot.wheels.set_velocity(3,7)
                    elseif robot.range_and_bearing[i].horizontal_bearing < 0 and
                            robot.range_and_bearing[i].horizontal_bearing >= -0.5 then
                        robot.wheels.set_velocity(10,9)
                    elseif robot.range_and_bearing[i].horizontal_bearing < -0.5 then
                        robot.wheels.set_velocity(7,3)
                    end
                elseif robot.range_and_bearing[i].range < 75 then

                    if #robot.colored_blob_omnidirectional_camera > 0 then
                        dist = 100
                        this_ang =  robot.colored_blob_omnidirectional_camera[1].angle
                        for i = 1, #robot.colored_blob_omnidirectional_camera do
                            --log(i)
                            if  (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and--small_disc
                                robot.colored_blob_omnidirectional_camera[i].color.green == 255 and
                                robot.colored_blob_omnidirectional_camera[i].color.blue == 255) then


                                dist = robot.colored_blob_omnidirectional_camera[i].distance
                                this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                --log(dist)
                                if dist > 30 and this_ang > 0 then
                                    robot.wheels.set_velocity(3,7)
                                elseif dist > 30 and this_ang < 0 then
                                    robot.wheels.set_velocity(7,3)
                                elseif dist < 30 and dist > 29 then
                                    robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                elseif dist < 29 and dist > .1 then
                                    int_state = "grab_robot"
                                end
                            elseif (robot.colored_blob_omnidirectional_camera[i].color.red == 165 and--large_disc
                                  robot.colored_blob_omnidirectional_camera[i].color.green == 42 and
                                  robot.colored_blob_omnidirectional_camera[i].color.blue == 42) or
                                  (robot.colored_blob_omnidirectional_camera[i].color.red == 160 and--small box
                                  robot.colored_blob_omnidirectional_camera[i].color.green == 32 and
                                  robot.colored_blob_omnidirectional_camera[i].color.blue == 240) then

                                    dist = robot.colored_blob_omnidirectional_camera[i].distance
                                    this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                    --log(dist)
                                    if dist > 40 and this_ang > 0 then
                                        robot.wheels.set_velocity(3,7)
                                    elseif dist > 40 and this_ang < 0 then
                                        robot.wheels.set_velocity(7,3)
                                    elseif dist < 40 and dist > 39 then
                                        robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                    elseif dist < 39 and dist > .1 then
                                        int_state = "grab_robot"
                                    end
                            elseif (robot.colored_blob_omnidirectional_camera[i].color.red == 255 and--large_box
                                    robot.colored_blob_omnidirectional_camera[i].color.green == 140 and
                                    robot.colored_blob_omnidirectional_camera[i].color.blue == 0) and
                                    robot.colored_blob_omnidirectional_camera[i].distance < dist then

                                      dist = robot.colored_blob_omnidirectional_camera[i].distance
                                      this_ang = robot.colored_blob_omnidirectional_camera[i].angle
                                      --log(dist)
                                      if dist > 50 and this_ang > 0 then
                                          robot.wheels.set_velocity(3,7)
                                      elseif dist > 50 and this_ang < 0 then
                                          robot.wheels.set_velocity(7,3)
                                      elseif dist < 50 and dist > 49 then
                                          robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
                                      elseif dist < 49 and dist > .1 then
                                          int_state = "grab_robot"
                                      end

                            end
                        end
                    end
                end
            end
        end
    end
end
--------------------------------------------------------------------------------
function grab_robot()
  grip_ang = 200
  x = robot.proximity[1]
  x.value = 0
  pos = 0
  for i = 1,24 do
      if robot.proximity[i].value >= x.value then
          x = robot.proximity[i]
          pos = i
      end
  end
  if x.value == 1 then
      grip_ang = x.angle
  elseif pos >= 1 and pos <= 12 then
      robot.wheels.set_velocity(0,0.75)
  elseif pos >= 13 and pos <= 24 then
      robot.wheels.set_velocity(0.75,0)
  end
  if grip_ang ~= 200 then
      --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
      robot.wheels.set_velocity(0,0)
      robot.turret.set_rotation(grip_ang)
      robot.gripper.lock_negative()
      count_time = count_time + 1
  end
  if count_time == 50 then
      robot.gripper.lock_negative()
      robot.turret.set_passive_mode()
      count_time = 0
      grabbed_msg = compose(root,6)
      robot.range_and_bearing.set_data(grabbed_msg)
      int_state = "final_orientation"
  end
end
--------------------------------------------------------------------------------
function final_orientation()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == 255 and
              robot.range_and_bearing[i].data[1] == parent.data[1]
              and(robot.range_and_bearing[i].data[3] == 7
              or robot.range_and_bearing[i].data[3] == 8
              or robot.range_and_bearing[i].data[3] == 9
              or robot.range_and_bearing[i].data[3] == 10) then

                    direction = robot.range_and_bearing[i].data[3]
            end

        end
    end
  --  log(direction)
    if direction == 7 then --go top/north
        if (sign == 1 and angle < 0.1) or
            (sing == -1 and angle > 6.1) then

            ready_to_go_msg = compose(parent.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 8 then --go left/west
        if (sign == 1 and angle > 1.4 and angle < 1.6) or
            (sign == -1 and angle > 4.6 and angle < 4.7) then

            ready_to_go_msg = compose(parent.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 9 then --go down/south
        if angle > 3 and angle < 3.2 then
            ready_to_go_msg = compose(parent.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 10 then --go right/west
        if (sign == 1 and angle > 4.6 and angle < 4.7) or
            (sign == -1 and angle > 1.4 and angle < 1.6) then
            ready_to_go_msg = compose(parent.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            --int_state = "home"
        else
            robot.turret.set_speed_control_mode()
            robot.turret.set_rotation_speed(-1.2)
            robot.wheels.set_velocity(-1,1)
        end
    end
    if #robot.range_and_bearing > 0 then
      for i = 1, #robot.range_and_bearing do
        if robot.range_and_bearing[i].data[1] == parent.data[1]
        and robot.range_and_bearing[i].data[2] == 255
        and robot.range_and_bearing[i].data[3] == 12 then
          int_state = "home"
        end
      end
    end
end
--------------------------------------------------------------------------------
--------------------------------common state------------------------------------
--------------------------------------------------------------------------------
function home()
    robot.wheels.set_velocity(10,10)
    --log(robot.motor_ground[1].value)
    ground = robot.motor_ground[1].value +
             robot.motor_ground[2].value +
             robot.motor_ground[3].value +
             robot.motor_ground[4].value
    --log(ground)
    if ground <= 0.25 then
        --log(ground)
        --log(count_time)
        count_time = count_time + 1
        if count_time >= 20 then
            count_time = 0
            robot.gripper.unlock()
            robot.turret.set_position_control_mode()
            state = "search"
            int_state ="listening"
        end
    end
end
--------------------------------------------------------------------------------
--[[
   Save Table to File
   Load Table from File
   v 1.0

   Lua 5.2 compatible

   Only Saves Tables, Numbers and Strings
   Insides Table References are saved
   Does not save Userdata, Metatables, Functions and indices of these
   ----------------------------------------------------
   table.save( table , filename )

   on failure: returns an error msg

   ----------------------------------------------------
   table.load( filename or stringtable )

   Loads a table that has been saved via the table.save function

   on success: returns a previously saved table
   on failure: returns as second argument an error msg
   ----------------------------------------------------

   Licensed under the same terms as Lua itself.
]]--
do
   -- declare local variables
   --// exportstring( string )
   --// returns a "Lua" portable version of the string
   local function exportstring( s )
      return string.format("%q", s)
   end

   --// The Save Function
   function table.save(  tbl,filename )
      local charS,charE = "   ","\n"
      local file,err = io.open( filename, "wb" )
      if err then return err end

      -- initiate variables for save procedure
      local tables,lookup = { tbl },{ [tbl] = 1 }
      file:write( "return {"..charE )

      for idx,t in ipairs( tables ) do
         file:write( "-- Table: {"..idx.."}"..charE )
         file:write( "{"..charE )
         local thandled = {}

         for i,v in ipairs( t ) do
            thandled[i] = true
            local stype = type( v )
            -- only handle value
            if stype == "table" then
               if not lookup[v] then
                  table.insert( tables, v )
                  lookup[v] = #tables
               end
               file:write( charS.."{"..lookup[v].."},"..charE )
            elseif stype == "string" then
               file:write(  charS..exportstring( v )..","..charE )
            elseif stype == "number" then
               file:write(  charS..tostring( v )..","..charE )
            end
         end

         for i,v in pairs( t ) do
            -- escape handled values
            if (not thandled[i]) then

               local str = ""
               local stype = type( i )
               -- handle index
               if stype == "table" then
                  if not lookup[i] then
                     table.insert( tables,i )
                     lookup[i] = #tables
                  end
                  str = charS.."[{"..lookup[i].."}]="
               elseif stype == "string" then
                  str = charS.."["..exportstring( i ).."]="
               elseif stype == "number" then--[[
   Save Table to File
   Load Table from File
   v 1.0

   Lua 5.2 compatible

   Only Saves Tables, Numbers and Strings
   Insides Table References are saved
   Does not save Userdata, Metatables, Functions and indices of these
   ----------------------------------------------------
   table.save( table , filename )

   on failure: returns an error msg

   ----------------------------------------------------
   table.load( filename or stringtable )

   Loads a table that has been saved via the table.save function

   on success: returns a previously saved table
   on failure: returns as second argument an error msg
   ----------------------------------------------------

   Licensed under the same terms as Lua itself.
]]--
do
   -- declare local variables
   --// exportstring( string )
   --// returns a "Lua" portable version of the string
   local function exportstring( s )
      return string.format("%q", s)
   end

   --// The Save Function
   function table.save(  tbl,filename )
      local charS,charE = "   ","\n"
      local file,err = io.open( filename, "wb" )
      if err then return err end

      -- initiate variables for save procedure
      local tables,lookup = { tbl },{ [tbl] = 1 }
      file:write( "return {"..charE )

      for idx,t in ipairs( tables ) do
         file:write( "-- Table: {"..idx.."}"..charE )
         file:write( "{"..charE )
         local thandled = {}

         for i,v in ipairs( t ) do
            thandled[i] = true
            local stype = type( v )
            -- only handle value
            if stype == "table" then
               if not lookup[v] then
                  table.insert( tables, v )
                  lookup[v] = #tables
               end
               file:write( charS.."{"..lookup[v].."},"..charE )
            elseif stype == "string" then
               file:write(  charS..exportstring( v )..","..charE )
            elseif stype == "number" then
               file:write(  charS..tostring( v )..","..charE )
            end
         end

         for i,v in pairs( t ) do
            -- escape handled values
            if (not thandled[i]) then

               local str = ""
               local stype = type( i )
               -- handle index
               if stype == "table" then
                  if not lookup[i] then
                     table.insert( tables,i )
                     lookup[i] = #tables
                  end
                  str = charS.."[{"..lookup[i].."}]="
               elseif stype == "string" then
                  str = charS.."["..exportstring( i ).."]="
               elseif stype == "number" then
                  str = charS.."["..tostring( i ).."]="
               end

               if str ~= "" then
                  stype = type( v )
                  -- handle value
                  if stype == "table" then
                     if not lookup[v] then
                        table.insert( tables,v )
                        lookup[v] = #tables
                     end
                     file:write( str.."{"..lookup[v].."},"..charE )
                  elseif stype == "string" then
                     file:write( str..exportstring( v )..","..charE )
                  elseif stype == "number" then
                     file:write( str..tostring( v )..","..charE )
                  end
               end
            end
         end
         file:write( "},"..charE )
      end
      file:write( "}" )
      file:close()
   end

   --// The Load Function
   function table.load( sfile )
      local ftables,err = loadfile( sfile )
      if err then return _,err end
      local tables = ftables()
      for idx = 1,#tables do
         local tolinki = {}
         for i,v in pairs( tables[idx] ) do
            if type( v ) == "table" then
               tables[idx][i] = tables[v[1]]
            end
            if type( i ) == "table" and tables[i[1]] then
               table.insert( tolinki,{ i,tables[i[1]] } )
            end
         end
         -- link indices
         for _,v in ipairs( tolinki ) do
            tables[idx][v[2]],tables[idx][v[1]] =  tables[idx][v[1]],nil
         end
      end
      return tables[1]
   end
-- close do
end

-- ChillCode
                  str = charS.."["..tostring( i ).."]="
               end

               if str ~= "" then
                  stype = type( v )
                  -- handle value
                  if stype == "table" then
                     if not lookup[v] then
                        table.insert( tables,v )
                        lookup[v] = #tables
                     end
                     file:write( str.."{"..lookup[v].."},"..charE )
                  elseif stype == "string" then
                     file:write( str..exportstring( v )..","..charE )
                  elseif stype == "number" then
                     file:write( str..tostring( v )..","..charE )
                  end
               end
            end
         end
         file:write( "},"..charE )
      end
      file:write( "}" )
      file:close()
   end

   --// The Load Function
   function table.load( sfile )
      local ftables,err = loadfile( sfile )
      if err then return _,err end
      local tables = ftables()
      for idx = 1,#tables do
         local tolinki = {}
         for i,v in pairs( tables[idx] ) do
            if type( v ) == "table" then
               tables[idx][i] = tables[v[1]]
            end
            if type( i ) == "table" and tables[i[1]] then
               table.insert( tolinki,{ i,tables[i[1]] } )
            end
         end
         -- link indices
         for _,v in ipairs( tolinki ) do
            tables[idx][v[2]],tables[idx][v[1]] =  tables[idx][v[1]],nil
         end
      end
      return tables[1]
   end
-- close do
end
