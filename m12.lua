function ReverseTable(t)
    local reversedTable = {}
    local itemCount = #t
    for k, v in ipairs(t) do
        reversedTable[itemCount + 1 - k] = v
    end
    return reversedTable
end

mat = {1, 7, 5, 3, 2}

table.sort(mat)
for i = 1, #mat do
  print(mat[i])
end
mat = ReverseTable(mat)
for i = 1, #mat do
  print(mat[i])
end
